<?php

namespace GenrWork;

use a4smanjorg5\Utils\Permissions;
use a4smanjorg5\Utils\Role;
use a4smanjorg5\Utils\OwnerRole;
use GenrWork\Tim\Contracts\AddsTeamMembers;
use GenrWork\Tim\Contracts\CreatesTeams;
use GenrWork\Tim\Contracts\DeletesTeams;
use GenrWork\Tim\Contracts\UpdatesTeamNames;

class Tim
{
    /**
     * The roles that are available to assign to users.
     *
     * @var array
     */
    public static $roles = [];

    public static function defaultKey() {
        if (func_num_args() > 0) {
            static::$defaultRole = (string)func_get_arg(0);
            return new static;
        }
        return static::$defaultRole;
    }

    /**
     * The permissions that are available to define role.
     */
    public static function permissions() {
        if (!static::$p) {
            static::$p = new Permissions()
                    ->add('read')
                    ->add('manage-member')
                    ->add('update')
                    ->add('delete')
                    ->add('as-team');
        }
        return static::$p;
    }

    /**
     * Determine if any permissions have been registered with Tim.
     *
     * @return bool
     */
    public static function hasPermissions() {
        return !is_null(static::$p);
    }

    /**
     * Find the role with the given key.
     *
     * @param  string  $key
     * @return \a4smanjorg5\Utils\Role
     */
    public static function findRole($key) {
        return static::$roles[$key] ?? null;
    }

    /**
     * Define a role.
     *
     * @param  string  $key
     * @param  string  $name
     * @param  array  $permissions
     * @return \a4smanjorg5\Utils\Role
     */
    public static function role($key, $name, $permissions) {
        $permissions = static::permissions()->available($permissions);
        if (!count(static::$roles))
            static::ownerRole();
        return tap(new Role($key, $name, $permissions), function($role) {
            static::$roles[$role->key] = $role;
            if (! static::$defaultRole)
                static::$defaultRole = $role->key;
        });
    }

    /**
     * Define owner role.
     *
     * @return \a4smanjorg5\Utils\OwnerRole
     */
    public static function ownerRole($name = 'Owner') {
        if (count(static::$roles))
            return static::$roles[Role::OWNER_KEY];
        return tap(new OwnerRole($name), function($role) {
            static::$roles[$role->key] = $role;
        });
    }
    public static function rolesKey() {
        return array_keys(static::$roles);
    }

    /**
     * Register a class / callback that should be used to create teams.
     *
     * @param  string  $class
     */
    public static function createTeamsUsing($class) {
        return app()->singleton(CreatesTeams::class, $class);
    }

    /**
     * Register a class / callback that should be used to update team names.
     *
     * @param  string  $class
     */
    public static function updateTeamNamesUsing($class) {
        return app()->singleton(UpdatesTeamNames::class, $class);
    }

    /**
     * Register a class / callback that should be used to add team members.
     *
     * @param  string  $class
     */
    public static function addTeamMembersUsing($class) {
        return app()->singleton(AddsTeamMembers::class, $class);
    }

    /**
     * Register a class / callback that should be used to delete teams.
     *
     * @param  string  $class
     */
    public static function deleteTeamsUsing($class) {
        return app()->singleton(DeletesTeams::class, $class);
    }

    /**
     * The permissions that exist within the application.
     *
     * @var \a4smanjorg5\Utils\Permissions
     */
    protected static $p;

    protected static $defaultRole;
}

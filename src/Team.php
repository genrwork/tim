<?php

namespace GenrWork\Tim;

use Illuminate\Database\Eloquent\Model;

abstract class Team extends Model
{
    /**
     * Get the owners of the team.
     */
    public function owners()
    {
        return $this->users->where('role', Role::OWNER_KEY);
    }

    /**
     * Get all of the users that belong to the team.
     */
    public function users()
    {
        return $this->belongsToMany(app_model('User', false), app_model('Membership', false))
                        ->withPivot('role')
                        ->withTimestamps()
                        ->as('membership');
    }

    /**
     * Determine if the given user belongs to the team.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function hasUser($user)
    {
        return $this->users->contains($user) || $user->ownsTeam($this);
    }

    /**
     * Determine if the given email address belongs to a user on the team.
     *
     * @param  string  $email
     * @return bool
     */
    public function hasUserWithEmail(string $email)
    {
        return $this->users->contains(function ($user) use ($email) {
            return $user->email === $email;
        });
    }

    /**
     * Determine if the given username belongs to a user on the team.
     *
     * @param  string  $username
     * @return bool
     */
    public function hasUserWithUsername(string $username)
    {
        return $this->users->contains(function ($user) use ($username) {
            return $user->username === $username;
        });
    }

    /**
     * Determine if the given user has the given permission on the team.
     *
     * @param  \App\Models\User  $user
     * @param  string  $permission
     * @return bool
     */
    public function userHasPermission($user, $permission)
    {
        return $user->hasTeamPermission($this, $permission);
    }

    /**
     * Remove the given user from the team.
     *
     * @param  \App\Models\User  $user
     */
    public function removeUser($user)
    {
        $this->users()->detach($user);
    }

    /**
     * Purge all of the team's resources.
     */
    public function purge()
    {
        $this->users()->detach();
        $this->delete();
    }
}

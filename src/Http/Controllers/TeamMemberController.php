<?php

namespace GenrWork\Tim\Http\Controllers;

use Illuminate\Routing\Controller;
use GenrWork\Tim\Actions\RemoveTeamMember;
use GenrWork\Tim\Actions\UpdateTeamMemberRole;
use GenrWork\Tim\Contracts\AddsTeamMembers;

class TeamMemberController extends Controller
{
    /**
     * Add a new team member to a team.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $teamId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($request, $teamId)
    {
        $team = app_model('Team')->findOrFail($teamId);

        app(AddsTeamMembers::class)->add(
            $request->user(),
            $team,
            $request->email ?: '',
            $request->role
        );

        return back(303);
    }

    /**
     * Update the given team member's role.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $teamId
     * @param  int  $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($request, $teamId, $userId)
    {
        app(UpdateTeamMemberRole::class)->update(
            $request->user(),
            app_model('Team')->findOrFail($teamId),
            $userId,
            $request->role
        );

        return back(303);
    }

    /**
     * Remove the given user from the given team.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $teamId
     * @param  int  $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($request, $teamId, $userId)
    {
        $team = app_model('Team')->findOrFail($teamId);

        app(RemoveTeamMember::class)->remove(
            $request->user(),
            $team,
            $user = app_model('User')->find($userId)
        );

        if ($request->user()->id === $user->id) {
            return redirect(config('fortify.home'));
        }

        return back(303);
    }
}

<?php

namespace GenrWork\Tim;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../stubs/TimServiceProvider.php' => app_path('Providers/TimServiceProvider.php'),
                __DIR__ . '/../stubs/Actions/' => app_path('Actions/'),
                __DIR__ . '/../stubs/Models/' => app_path('Models/'),
            ]);
        }
    }
}

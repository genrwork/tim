<?php

namespace App\Models;

use GenrWork\Tim\Events\TeamCreated;
use GenrWork\Tim\Events\TeamDeleted;
use GenrWork\Tim\Events\TeamUpdated;
use GenrWork\Tim\Team as GenrworkTeam;

class Team extends GenrworkTeam
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => TeamCreated::class,
        'updated' => TeamUpdated::class,
        'deleted' => TeamDeleted::class,
    ];
}

<?php

namespace App\Providers;

use App\Actions\Tim\AddTeamMember;
use App\Actions\Tim\CreateTeam;
use App\Actions\Tim\DeleteTeam;
use App\Actions\Tim\DeleteUser;
use App\Actions\Tim\UpdateTeamName;
use GenrWork\Tim;
use Illuminate\Support\ServiceProvider;

class TimServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configurePermissions();

        Tim::createTeamsUsing(CreateTeam::class);
        Tim::updateTeamNamesUsing(UpdateTeamName::class);
        Tim::addTeamMembersUsing(AddTeamMember::class);
        Tim::deleteTeamsUsing(DeleteTeam::class);
        Tim::deleteUsersUsing(DeleteUser::class);
    }

    /**
     * Configure the roles and permissions that are available within the application.
     *
     * @return void
     */
    protected function configurePermissions()
    {
        $p = Tim::permissions();
        Tim::defaultApiTokenPermissions($p->available('read'));

        Tim::ownerRole(__('Administrator'))->description(__('Manage members and access all.'));
        Tim::role('member', __('Member'), $p->available('read'))->description(__('Join as a member.'));
        // Tim::defaultKey('member');
    }
}
